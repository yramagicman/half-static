<?php

use Illuminate\Database\Seeder;
use \App\Permission;
use \App\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'edit articles']);
        Permission::create(['name' => 'delete articles']);
        Permission::create(['name' => 'publish articles']);
        Permission::create(['name' => 'unpublish articles']);

        Permission::create(['name' => 'create comment']);
        Permission::create(['name' => 'edit comment']);
        Permission::create(['name' => 'delete comment']);

        Permission::create(['name' => 'access admin']);

        // create roles and assign created permissions

        // this can be done as separate statements
        $commenter = Role::create(['name' => 'commenter'])
              ->givePermissionTo(['create comment', 'edit comment']);

        // or may be done by chaining
        $moderator = Role::create(['name' => 'moderator'])
              ->givePermissionTo(['create comment', 'edit comment', 'delete comment']);

        $superadmin = Role::create(['name' => 'Super Admin']);
        $superadmin->givePermissionTo(Permission::all());

    }
}
