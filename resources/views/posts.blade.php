@extends('layouts.app')

@section('content')
    @foreach($posts as $post)
        <div class="post">
            <header>
                <h2>
                    <a href="/post/{{ $post->slug }}" class="post-title">{{ $post->title}}</a>
                </h2>
            </header>

            <section>
                {!! $post->post_summary !!}
            </section>
            <a href="/post/{{ $post->slug }}">Continue reading {{ $post->title}}</a>
        </div>
    @endforeach
        {{ $posts->links() }}
@endsection
