<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/manifest.js') }}" defer></script>
        <script src="{{ asset('js/vendor.js') }}" defer></script>
        <script src="{{ asset('js/app.js') }}" defer></script>


        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="container" id="app">
            <header class="masthead">
                <h1>
                    <a href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </h1>
            </header>
            <main class="content">
                @yield('content')
            </main>

            <nav class="main-nav">
                <div>
                    <h3>Search</h3>
                    <form method="get" action="{{ route('search') }}"
                                       class="search">
                        <input type="text" name="q" placeholder="search">
                    </form>
                </div>
                <h3>Posts</h3>
                <ul>
                @foreach($posts as $p)
                    <li><a href="{{ route('post',  $p->slug ) }}" title="{{
                    $p->title}}">{{
                    $p->title}}</a></li>
                @endforeach
                </ul>
                {{-- @if( ! empty( $categories ) && count($categories) > 0 ) --}}
                {{--     <h3>Categories</h3> --}}
                {{--     <ul> --}}
                {{--         @foreach( $categories as $route ) --}}
                {{--             <li> --}}
                {{--                 <a href="{{ route('category', $route->slug) }}"> --}}
                {{--                     {{ $route->name }} --}}
                {{--                 </a> --}}
                {{--             </li> --}}
                {{--         @endforeach --}}
                {{--     </ul> --}}
                {{-- @endif --}}
                {{-- @if( ! empty( $series ) && count($series) > 0 ) --}}
                {{--     <h3>Series</h3> --}}
                {{--     <ul> --}}
                {{--         @foreach( $series as $route ) --}}
                {{--             <li> --}}
                {{--                 <a href="{{ route('series_post_list', $route->slug) }}"> --}}
                {{--                     {{ $route->name }} --}}
                {{--                 </a> --}}
                {{--             </li> --}}
                {{--         @endforeach --}}
                {{--     </ul> --}}
                {{-- @endif --}}
                {{-- @if( ! empty( $tags ) && count($tags) > 0) --}}
                {{--     <h3>Tags</h3> --}}
                {{--     <ul> --}}
                {{--         @foreach( $tags as $route ) --}}
                {{--             <li> --}}
                {{--                 <a href="{{ route('tag', $route->slug) }}"> --}}
                {{--                     {{ $route->name }} --}}
                {{--                 </a> --}}
                {{--             </li> --}}
                {{--         @endforeach --}}
                {{--     </ul> --}}
                {{-- @endif --}}
            </nav>
        </div>
    </body>
</html>
