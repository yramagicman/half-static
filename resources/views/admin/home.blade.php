@extends('layouts.admin')

@section('content')
    <div class="dashboard">
        <ul>
            <li>
                <a href="{{ route('admin.posts') }}"> manage posts</a>
            </li>
            <li>
                <a href="{{ route('admin.meta_admin', 'category') }}"> manage categories</a>
            </li>
            <li>
                <a href="{{ route('admin.meta_admin', 'tag') }}"> manage tags</a>
            </li>
            <li>
                <a href="{{ route('admin.meta_admin', 'series') }}"> manage series</a>
            </li>
        </ul>
    </div>
<passport-clients></passport-clients>
<passport-authorized-clients></passport-authorized-clients>
<passport-personal-access-tokens></passport-personal-access-tokens>

@endsection
