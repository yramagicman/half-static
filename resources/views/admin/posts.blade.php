@extends('layouts.admin')

@section('content')
    <div class="container">
        <a href="{{route('admin.home')}}">back</a>
        <table>
            <thead>
                <tr>
                    <td> Post Name</td>
                    <td> Post Slug</td>
                    <td colspan="2"> Actions</td>
                </tr>
            </thead>
            @foreach( $posts as $post)
                <tr>
                <td><a href="{{ route('post', $post->slug)}}"> {{
                $post->title}}</a></td>
                    <td> {{ $post->slug}}</td>
                    <td> <a href="{{ route('admin.post_edit',
                    $post->slug)}}" > edit </a></td>
                    <td> <delete-btn url="{{ route('api.post_delete',
                    $post->slug)}}" > </delete-btn></td>
                </tr>

            @endforeach
        </table>
    </div>
@endsection
