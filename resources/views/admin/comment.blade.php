@extends('layouts.admin')

@section('content')
    <div class="container">
	<a href="{{route('admin.home')}}">back</a>
	<form method="post"
	      action="{{route('edit_comment',  $comments->getPost($comments)->slug )}}">
	    @csrf
	    <textarea>{{$comments->comment}}</textarea>
	    <input type="hidden" value="{{ $comments->id }}" name="id">
	    <input type="submit">
	</form>
    </div>
@endsection
