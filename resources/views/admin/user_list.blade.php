@extends('layouts.admin')

@section('content')
    <div class="container">
        <a href="{{route('admin.home')}}">back</a>
        <table>
            <thead>
                <tr>
                    <td> user name</td>
                    <td> roles</td>
                    <td colspan="2">Actions</td>
                </tr>
            </thead>
            @if(count( $users ))
                @foreach( $users as $user)
                    <tr>
                        <td> {{ $user->name}}</td>
                        <td>
                            @foreach( $user->roles as $role )
                                {{ $role->name }}
                            @endforeach
                        </td>
                        <td> <a href="{{ route('admin.user_edit', $user->id)}}" > edit </a></td>
                    </tr>
                @endforeach
            @endif
        </table>
    </div>
@endsection
