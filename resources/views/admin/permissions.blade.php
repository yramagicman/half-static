@extends('layouts.admin')

@section('content')
    <div class="container">
        <a href="{{route('admin.home')}}">back</a>
        <table>
            <thead>
                <tr>
                    <td> {{ $meta_name}} name</td>
                    <td> {{ $meta_name}} slug</td>
                    <td colspan="2">Actions</td>
                </tr>
            </thead>
            @if(count( $meta ))
                @foreach( $meta as $m)
                    <tr>
                        <td> {{ $m->name}}</td>
                        <td> {{ $m->slug}}</td>
                        <td> <a href="{{ route('admin.meta_delete',
                        [ 'category', $m->slug ] )}}" > delete </a></td>
                    </tr>
                @endforeach
            @endif
        </table>
        @if ( $errors->any() )
            @foreach($errors->all() as $error)
                {{ $error }}
            @endforeach
        @endif
        <form method="post" action="{{ route($action_url,
        strtolower( $meta_name )) }}">
            @csrf
            <label for="name">{{ $meta_name }}
                <input type="text" name="name" id="name">
            </label>
            <input type="submit" value="Add {{$meta_name}}">
        </form>
    </div>
@endsection
