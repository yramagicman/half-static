
@extends('layouts.admin')

@section('content')
    <div class="container">
        <a href="{{route('admin.home')}}">back</a>
        <table>
            <thead>
                <tr>
                    <td>Post Name</td>
                    <td>User</td>
                    <td>Comment excerpt</td>
                    <td colspan=2>Actions</td>
                </tr>
            </thead>
            @foreach( $comments as $comment)
                <tr>
                    <td>
                        <a href="{{ route('post', $comment->getPost($comment)->slug )}}">
                            {{ $comment->getPost($comment)->title }}
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('admin.user_edit', $comment->user()->first()->id )}}">
                            {{ $comment->user()->first()->name }}
                        </a>
                    </td>
                    <td>
                        {!! $comment->excerpt($comment->comment) !!}
                    </td>
                    <td>
                        <a href="{{ route('admin.comments_edit', $comment->id
                        )}}">edit </a>
                    </td>
                    <td>
                        <delete-btn url="{{ route('admin.comments_delete',
                        $comment->id )}}"></delete-btn>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
