@extends('layouts.admin')

@section('content')
    <div class="container">
        <a href="{{route('admin.home')}}">back</a>
        <table>
            <thead>
                <tr>
                    <td> {{ $meta_name}} name</td>
                    <td> {{ $meta_name}} slug</td>
                    <td colspan="2">Actions</td>
                </tr>
            </thead>
            @if(count( $meta ))
                @foreach( $meta as $m)
                    <tr>
                        <td> {{ $m->name}}</td>
                        <td> {{ $m->slug}}</td>
                        <td> <delete-btn url="{{ route('api.meta_delete',
                        [strtolower( $meta_name ), $m->slug ])}}" > </delete-btn></td>
                    </tr>
                @endforeach
            @endif
        </table>
        <form method="post" action="{{ route('admin.meta_create',
        strtolower( $meta_name )) }}">
            @csrf
            <label for="name">{{ $meta_name }}
                <input type="text" name="name" id="name">
            </label>
            <input type="submit" value="Add {{$meta_name}}">
        </form>
    </div>
@endsection
