@extends('layouts.admin')

@section('content')
    <div class="container">
        <a href="{{route('admin.home')}}">back</a>
        <h2>{{ $user->name }}</h2>
        {{ $user->api_token }}
        <form class="permissions" method="post" action="{{
        route('admin.user_update', $user->id) }}">
        @csrf
        <h2> Available Roles </h2>
        <ul>
            @foreach($roles as $role)
                @if( in_array($role->name, $userRoles) )
                    <li> {{ $role->name }}

                        <input type="radio"
                               value="{{ $role->name }}"
                               name="role" checked >
                    </li>
                @else
                    <li> {{ $role->name }}
                        <input type="radio"
                               value="role['{{ $role->name }}']"
                               name="role" >
                    </li>
                @endif
            @endforeach
        </ul>
        <h2> Available Permissions </h2>
        <ul>
            @foreach($permissions as $permission)
                @if( in_array($permission->name, $userPerms) )
                    <li>
                        {{ $permission->name }}
                        <input type="checkbox"
                               value="{{ $permission->name }}"
                               name="permission[]" checked >

                    </li>
                @else
                    <li>
                        {{ $permission->name }}
                        <input type="checkbox"
                               value="{{ $permission->name }}"
                               name="permission[]">
                    </li>
                @endif
            @endforeach
        </ul>
        <input type="submit">
        </form>
    </div>
@endsection
