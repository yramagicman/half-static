@extends('layouts.admin')

@section('content')
    <div class="content">
        <a href="{{route('admin.home')}}">back</a>
        <form method="post" action="{{ route('admin.post_update',
        $post->slug)}}">
        @csrf
        <div class="form-group">
            <label for="post_title">Post Title</label>
            <input type="text" name="post_title" value="{{ $post->title }}">

        </div>
        <div>
            <strong>Post Slug:</strong> {{ $post->slug }}
        </div>

        <div>
            @if($post->categories())
                <h2>Categories</h2>
                <ul>
                    @foreach($categories as $cat)
                        <li>
                            @if($post->categories()->where('name',
                                $cat->name)->first())
                                <input type="checkbox"
                                       name="category_select[]" value="{{
                                       $cat->slug}}" checked> {{ $cat->name}}
                                   @else

                                       <input type="checkbox"
                                              name="category_select[]" value="{{
                                              $cat->slug}}"> {{ $cat->name}}
                                          @endif
                        </li>
                    @endforeach
                </ul>
            @endif
            <div>
                @if(count( $post->categories()->get()->toArray() ) > 1)
                    <h2> Applied Categories </h2>
                    @if (count($post->categories()->get()) > 1)
                        @foreach($post->categories()->get() as $pcat)
                            {{ $pcat->name}},
                        @endforeach
                    @else
                        {{ $post->categories()->first()->name }}
                    @endif
                @endif
            </div>
        </div>
        <div>
            @if ($post->tags())
                <h2>Tags</h2>
                <ul>
                    @foreach($tags as $tag)
                        <li>
                            @if($post->tags()->where('name',
                                $tag->name)->first())
                                <input type="checkbox"
                                       name="tag_select[]" value="{{
                                       $tag->slug}}" checked> {{ $tag->name}}
                                   @else

                                       <input type="checkbox"
                                              name="tag_select[]" value="{{
                                              $tag->slug}}"> {{ $tag->name}}
                                          @endif
                        </li>
                    @endforeach
                </ul>
                @endif
                <div>
                    @if (count( $post->tags()->get()->toArray() ) > 1)
                        <h2> Applied Tags </h2>
                        @if (count($post->tags()->get()) > 1)
                            @foreach($post->tags()->get() as $ptag)
                                {{ $ptag->name}}
                            @endforeach
                        @else
                            {{ $post->tags()->first()->name }}
                        @endif
                    @endif
                </div>
        </div>
        <div>
            <h2>Series</h2>
            <ul>
                @foreach($series as $s)
                    <li>
                        @if($post->series()->where('name',
                            $s->name)->first())
                            <input type="checkbox"
                                   name="series_select[]" value="{{
                                   $s->slug}}" checked> {{ $s->name}}
                               @else

                                   <input type="checkbox"
                                          name="segory_select[]" value="{{
                                          $s->slug}}"> {{ $s->name}}
                                      @endif
                    </li>
                @endforeach
            </ul>
            <div>
                <h2> Applied Series </h2>
                @foreach($post->series()->get() as $pseries)
                    {{ $pseries->name}}
                @endforeach
            </div>
        </div>
        <input type="submit" value="update">
        </form>
    </div>
@endsection
