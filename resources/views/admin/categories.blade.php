@extends('layouts.admin')

@section('content')
    <div class="container">
        <a href="{{route('admin.home')}}">back</a>
        <table>
            <thead>
                <tr>
                    <td> category name</td>
                    <td> category slug</td>
                </tr>
            </thead>
            @if(count( $categories ))
                @foreach( $categories as $category)
                    <tr>
                        <td> {{ $category->name}}</td>
                        <td> {{ $category->slug}}</td>
                        <td> <a href="{{ route('admin.category_delete',
                        $category->slug)}}" > delete </a></td>
                    </tr>
                @endforeach
            @endif
        </table>
        <form method="post" action="{{ url('/admin/categories/') }}">
            @csrf
            <label for="name">Category
                <input type="text" name="name" id="name">
            </label>
            <input type="submit" value="Add category">
        </form>

    </div>
    <script type="text/javascript">
        let btn = document.querySelectorAll('.delcat');
btn = Array.from(btn);
btn.map(function(b) {
    b.addEventListener('click', function(e) {
        fetch('/admin/categories', {method: 'DELETE'});
    });
});
    </script>
@endsection
