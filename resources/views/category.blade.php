
@extends('layouts.app')

@section('content')
    <header>Categories</header>
    <ul>
        @foreach($meta as $category)
            <li><a href="{{route('category', $category->slug)}}">{{
            $category->name}}</a></li>

        @endforeach
    </ul>
@endsection
