
@extends('layouts.app')

@section('content')
    <div class="post">
        <header>
            <h2>{{ $post->title}}</h2>
        </header>
        <section>
            {!! $post->post !!}
        </section>

        @if (count($post->comments))
            <section>
                <h3>Comments</h3>
                @foreach($post->comments as $comment)
                    <div class="comment-container">
                        <div class="comment-text">
                            {!! $comment->parse($comment->comment) !!}
                        </div>
                        <div class="comment-commenter">
                            {{ $comment->user->name }}
                        </div>
                        @can('edit comment')
                            <edit-comment
                                content="{{ $comment->decode($comment->comment) }}"
                                route="{{ route('edit_comment', $post->slug) }}"
                                id="{{ $comment->id }}"
                                > </edit-comment>
                            @endcan
                    </div>
                @endforeach
            </section>
        @endif
        @can('create comment')
            <h3> Post New Comment </h3>
            <form method="post" action="{{ route('post_comment', $post->slug) }}">
                @csrf
                <textarea name="comment" class="comment"></textarea>
                {{-- @honeypot --}}
                <input type="submit">
            </form>
        @endcan
    </div>
@endsection
