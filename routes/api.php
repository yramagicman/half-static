<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->name('api.')->group(function(){
    Route::get('/comments/{post}', 'CommentController@show' )->name('show_comments');
    Route::post('/comments/{post}', 'CommentController@show' )->name('post_comments');
    Route::delete('/meta/{value}/{slug}', 'MetaAdmin@destroy')->name('meta_delete');
    Route::delete('/posts/{slug}', 'PostAdmin@destroy')->name('post_delete');
    Route::delete('/permissions/{id}', 'PermissionController@destroy')->name('permission_delete');
    Route::delete('/roles/{id}', 'RoleController@destroy')->name('role_delete');
    Route::delete('/users/{id}', 'UserController@destroy')->name('user_delete');
    Route::delete('/comments/{id}', 'CommentController@destroy')->name('comments_delete');

});
