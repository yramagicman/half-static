<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', 'PostController@index')->name('index');
Route::get('/post/{slug}', 'PostController@show')->name('post');
Route::get('/categories/{slug}', 'CategoryController@show')->name('category');

Route::get('/series/', 'SeriesController@index')->name('series_list');
Route::get('/series/{slug}', 'SeriesController@show')->name('series_post_list');

Route::get('/tags/{slug}', 'TagController@show')->name('tag');

Route::post('/comment/{post}', 'CommentController@store')->name('post_comment');
Route::post('/comment/edit/{post}', 'CommentController@update')->name('edit_comment');

Route::get('/search', 'SearchController@index')->name('search');
Auth::routes();

Route::prefix('admin')
    ->name('admin.')
    ->middleware(['auth', 'permission:access admin'])
    ->group(function() {

        Route::get('/', 'HomeController@index')->name('home');

        Route::get('/meta', 'MetaAdmin@index')->name('meta_list');
        Route::get('/meta/{value}', 'MetaAdmin@show')->name('meta_admin');
        Route::post('/meta/{value}/', 'MetaAdmin@store')->name('meta_create');

        Route::get('/posts', 'PostAdmin@index')->name('posts');
        Route::get('/posts/{slug}', 'PostAdmin@show')->name('post_edit');
        Route::post('/posts/{slug}', 'PostAdmin@update')->name('post_update');

        Route::get('/permissions', 'PermissionController@index')->name('permission_list');
        Route::post('/permissions/create', 'PermissionController@store')->name('permission_create');

        Route::get('/roles', 'RoleController@index')->name('role_list');
        Route::post('/roles/create', 'RoleController@store')->name('role_create');

        Route::get('/users/', 'UserController@index')->name('user_admin');
        Route::get('/users/{id}', 'UserController@show')->name('user_edit');
        Route::post('/users/{id}', 'UserController@update')->name('user_update');

        Route::get('/comments', 'CommentController@index')->name('comments_admin');
        Route::get('/comments/{id}', 'CommentController@edit')->name('comments_edit');
    });
