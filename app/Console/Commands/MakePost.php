<?php

namespace App\Console\Commands;

use \App\Http\Controllers\PostAdmin;
use \App\Util\PostMeta;
use Illuminate\Console\Command;
use Parsedown;


class MakePost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:make';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate post from markdown file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $directory = env('MARKDOWN_DIR');
        shell_exec('mkdir -p ' . $directory);
        chdir($directory);
        shell_exec('mkdir -p drafts new published');

        foreach(scandir('./new') as $file){
            if ($file != '.' && $file !='..'){
                $this->info("parsing $file");
                $parsedown = new Parsedown();
                $f_contents=file_get_contents("./new/$file");
                $post = preg_split('/\n/', $f_contents);
                $title = array_shift($post);
                $post_array = PostMeta::extractMeta( $post );
                $post= $post_array[0];
                $meta= $post_array[1];
                $p = $parsedown::instance()->text(implode("\n", $post));
                $title = trim( substr( $title, 1 ) );
                $this->info($title);
                PostAdmin::makePost($title, $p, $meta);
                $pub = implode(' ', scandir('./new'));
                rename("./new/$file", "./published/$file");
            }
        }
        $this->info($pub);
    }
}
