<?php

namespace App\Console\Commands;

use App\Role;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CreateSuperUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:superuser';
    private $pass;
    private $passConfirm;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = $this->ask('User name');
        $email = $this->ask('Email address');
        self::askPass();
        if ( $this->pass !== $this->passConfirm) {
            while ( $this->pass !== $this->passConfirm) {
                $this->error("passwords don't match");
                self::askPass();
            }
        }
        $user = User::create([
            'name' => $username,
            'email' => $email,
            'password' => Hash::make( $this->pass )]);
        $user->assignRole('Super Admin');
    }
    private function askPass() {
        $this->pass = $this->secret('password?');
        $this->passConfirm = $this->secret('confirm password?');
    }
}
