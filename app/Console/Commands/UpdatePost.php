<?php

namespace App\Console\Commands;

use \App\Http\Controllers\PostAdmin;
use \App\Post;
use \App\Util\PostMeta;
use Illuminate\Console\Command;
use Parsedown;

class UpdatePost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:update {slug}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update published post';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $directory = env('MARKDOWN_DIR');
        chdir($directory);
        $this->info('scanning directory');
        foreach(scandir('./published') as $file){
            if ($file != '.' && $file !='..'){
                $f_contents=file_get_contents("./published/$file");
                $post = preg_split('/\n/', $f_contents);
                $title = array_shift($post);
                $post_array = PostMeta::extractMeta( $post );
                $post= $post_array[0];
                $meta= $post_array[1];
                $title = trim( substr( $title, 1 ) );
                $slug = trim( $this->argument('slug') );
                $p_slug = trim( Post::clean($title) );
                $this->info($p_slug);
                $this->info($title);
                if ( $p_slug == $slug ) {
                    $this->info("update $slug");
                    $parsedown = new Parsedown();
                    $p = $parsedown::instance()->text(implode("\n", $post));
                    $this->info(PostAdmin::updatePost($title, $p, $meta));
                }
            }
        }
        $pub = implode(' ', scandir('./published'));
    }
}
