<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Meta;

class Category extends Model implements Meta
{

    public $fillable = ['name', 'slug',];
    public static function makeSlug($category){
        return str_replace(' ', '_', strtolower( $category ));
    }


}
