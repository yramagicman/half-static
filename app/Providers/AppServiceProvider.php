<?php

namespace App\Providers;

use \App\Category;
use \App\Series;
use \App\Tag;
use \App\Post;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.app', function($view) {
            $view->with([
                'categories'=> Category::all(),
                'series' => Series::all(),
                'tags' => Tag::all(),
                'posts' => Post::take(10)->get()
            ]);
        });
    }
}
