<?php
namespace App\Util;

use \App\Category;
use \App\Series;
use \App\Tag;
use \App\Post;
use Illuminate\Support\Facades\DB;

class PostMeta {


    public static function extractMeta($post) {
        $valid_meta= ['categories', 'category', 'tag', 'tags', 'series'];
        $meta_list = [];
        foreach($post as $line) {
            if( substr($line, 0, 2) == '--'  ) {
                $meta_str = explode(':', $line);
                $meta_value = trim( $meta_str[1] );
                $meta_name = strtolower( trim( $meta_str[0] ) );
                if (in_array(substr($meta_name, 3), $valid_meta )) {

                    $meta_list[substr( $meta_name, 3 )] = self::parseMeta($meta_name, $meta_value);
                    array_shift($post);
                }
            }
        }
        return [ $post, $meta_list ];
    }

    public static function assignMeta($class, $meta_list) {
        $metas = explode(',', $meta_list);
        $meta_objects = [];
        foreach ( $metas as $meta) {
            $meta = trim( strtolower($meta) );
            \Log::info($meta);
            $slug = $class::makeSlug(trim((  $meta ) ));
            $meta_objects[] = $class::firstOrNew(['name' => $meta, 'slug' => $slug]);
        }
        return $meta_objects;
    }

    public static function parseMeta($name, $value) {
        switch ( $name ) {
        case '-- category':
            //fall through
        case '-- categories':
            $cats = self::assignMeta(Category::class, $value);
            return $cats;
        case '-- tag':
            //fall through
        case '-- tags':
            $tags = self::assignMeta(Tag::class, $value);
            return $tags;
        case '-- series':
            $series = self::assignMeta(Series::class, $value);
            return $series;

        }
    }
    public static function getPostsFromMeta($table_str, $meta_id, $slug_id) {
        $d = DB::table($table_str)->where($meta_id, $slug_id)->select('post_id')->get()->toArray();
        $post_ids=[];
        foreach ($d as $key => $value) {
            $post_ids[] = $value->post_id;
        }

        $posts = Post::whereIn('id', $post_ids)->paginate(10);
        return $posts;
    }
}
