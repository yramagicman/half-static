<?php
namespace App\Util;

use \App\Category;
use \App\Series;
use \App\Tag;
use \App\Post;
use Illuminate\Support\Facades\DB;

class View {
    public static function getBaseView() {
        $baseView = [
            'categories' => self::catList(),
            'series' => self::seriesList(),
            'tags' => self::tagList(),
        ];
        return $baseView;
    }
    private static function catList() {
        return Category::all();
    }

    private static function seriesList() {
        return Series::all();
    }

    private static function tagList() {
        return Tag::all();
    }
}
