<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    public $fillable = ['date_created', 'date_updated', 'post', 'author', 'title', 'slug'];

    public static function clean($string) {
        if (strpos($string, ' ') !== false ) {

            $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        }

        $string = strtolower($string);
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function comments() {
        return $this->hasMany('\App\Comment');
    }
    public function categories() {
       return $this->belongsToMany('\App\Category');
    }

    public function tags() {
       return $this->belongsToMany('\App\Tag');
    }

    public function series() {
       return $this->belongsToMany('\App\Series');
    }

    public static function makeSummary($post) {
        $summary= array_chunk( preg_split('/\n/', $post), 2);
        $summary=$summary[0];
        return strip_tags(implode($summary), '<p><ol><ul><li><a><h1><h2><h3>');
    }

}
