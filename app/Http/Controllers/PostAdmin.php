<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use \App\Post;
use \App\Category;
use \App\Series;
use \App\Tag;

class PostAdmin extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $p = Post::select('title', 'slug')->get();
        return view('admin.posts', ['posts'=> $p]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $p = Post::where('slug', $id)->first();
        $c= Category::all();
        $s= Series::all();
        $t= Tag::all();

        return view('admin.post', ['post'=> $p,
            'categories'=>$c,
            'series'=>$s,
            'tags'=>$t
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $p = Post::where('slug', $id)->first();
        self::updatePostMeta($p, $request, Category::class, 'category_post', 'category_select', $p->categories());
        self::updatePostMeta($p, $request, Series::class, 'post_series', 'series_select', $p->series());
        self::updatePostMeta($p, $request, Tag::class, 'post_tag', 'tag_select', $p->tags());
        return redirect()->route('admin.posts');
    }

    private static function updatePostMeta($post, $request, $class, $table, $input, $fn) {
        DB::table($table)->where('post_id', $post->id)->delete();
        if ($request->get($input)) {
            foreach($request->get($input) as $series) {
                $c = $class::where('slug', $series)->first();
                $fn->save($c);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::where('slug', $id)->delete();
        return 'success';
    }

    public static function makePost($title,$content, $meta) {
        if (empty($meta)){
            $c = Category::where('slug', 'uncategorized')->first();
            if (empty( $c )) {
                $c= new Category(['name' => 'uncategorized', 'slug'=> 'uncategorized']);
                $c->save();
                $c = Category::where('slug', 'uncategorized')->first();
            }
        }
        $p = new Post();
        $p['author'] = 'Jonathan';
        $p['post'] = $content;
        $p['post_summary'] = $p->makeSummary($content);
        $p['title'] = $title;
        $p['slug'] = $p->clean($title);
        $p['date_created'] = Carbon::now();
        $p['date_updated'] = Carbon::now();
        $p->save();
        self::saveAllMeta($meta, $p);

    }
    private static function saveAllMeta($meta, $post) {

        self::saveMeta($meta, 'category', 'categories', $post->categories());
        self::saveMeta($meta, 'tag', 'tags', $post->tags());
        self::saveMeta($meta, 'series', 'series', $post->series());
    }

    private static function saveMeta($meta, $name, $plural_name, $fn) {

        if ( !empty($meta) ) {
            foreach ( $meta as $key => $m ) {
                if ( $key == $name || $key == $plural_name) {
                    foreach( $m as $n) {
                        $fn->save($n);
                    }
                }
            }
        }
    }


    public static function updatePost($title, $content, $meta) {
        $slug = Post::clean($title);
        $p = Post::where('slug', $slug)->first();
        $p['title'] = $title;
        $p['post'] = $content;
        $p['date_updated'] = Carbon::now();
        $p->save();
        self::saveAllMeta($meta, $p);
    }
}
