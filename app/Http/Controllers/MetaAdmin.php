<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Category;
use \App\Tag;
use \App\Series;
class MetaAdmin extends Controller
{
    private $class;
    private $meta_name;
    private $meta_slug;
    private $meta_plural_name;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request)
    {
        $url_array = explode('/', $request->url());
        if (count($url_array) > 5) {
            $id = $url_array[5];
            switch ( $id ) {
            case $id == 'category':
                $this->meta_name = 'Category';
                $this->meta_slug= $id;
                $this->meta__plural_name = 'Categories';
                $this->class = new Category();
                break;
            case $id == 'tag':
                $this->meta_slug= $id;
                $this->meta_name = 'Tag';
                $this->meta__plural_name = 'Tags';
                $this->class = new Tag();
                break;
            case $id == 'series':
                $this->meta_slug= $id;
                $this->meta_name = 'Series';
                $this->meta__plural_name = 'Series';
                $this->class = new Series();
                break;
            }
        }
    }
    public function index()
    {
        return 'hello';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $meta = $this->class;
        $meta['name'] = $request->get('name');
        $meta['slug'] = $this->class->makeSlug($request->get('name'));
        $this->class->save();
        return redirect()->route('admin.meta_admin', $this->meta_slug);

    }

    /**
     * Display the specified resource.
     *
     * @param  str  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->class) {

            return view('admin.meta', [ 'meta' => $this->class::all(), 'meta_name'=> $this->meta_name, 'meta_plural_name' => $this->meta_plural_name] );
        } else{
            return abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $slug)
    {
        $this->class::where('slug', $slug)->delete();
        return 'success';
    }
}
