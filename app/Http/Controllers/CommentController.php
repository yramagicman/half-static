<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $p = Comment::all();
        return view('admin.comments', ['comments'=> $p]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($slug, Request $request)
    {
        $comment = $request->validate([
            'comment'=>'required'
        ]);
        $userComment = htmlspecialchars($comment['comment']);
        $user = \Auth::user();
        $comment = new Comment();
        $comment['comment'] = $userComment;
        $comment['date_created'] = Carbon::now();
        $comment['date_updated'] = Carbon::now();
        $comment->save();
        $comment->user()->associate($user);
        $comment->save();
        $user['hashed_ip'] = hash('sha256', $request->ip());
        $user->save();

        $post = Post::where('slug', $slug)->first();
        $post->comments()->save($comment);
        return redirect()->route('post', $slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show($post, Comment $comment)
    {
        return $comment->where('id', $post)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Comment $comment)
    {
        $c = $comment->where('id', $id)->first();
        return view('admin.comment', ['comments'=> $c]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update($slug, Request $request, Comment $comment)
    {
        $r = $request->validate([
            'comment'=>'required|string',
            'id'=>'required|numeric'
        ]);

        $content = htmlspecialchars($r['comment']);
        $update = $comment->where('id', $r['id'])->first();
        $update['comment'] = $content;
        $update->save();

        return redirect()->route('post', $slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
