<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user_list', [
            'users' => User::with('roles')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id, User $user)
    {
        $selected = $user->where('id', $id)->first();
        $roles = Role::all();
        $userRoles = [];

        foreach ($roles as $role) {
            $userRoles[$role->name] = $selected->hasRole($role->name)? $role->name : false;
        }

        $permissions = Permission::all();
        $userPerms = [];
        foreach($permissions as $perm) {
            $userPerms[$perm->name] = $selected->hasPermissionTo($perm->name)? $perm->name : false;
        }

        return view('admin.user_details', [
            'user' => $user->where('id', $id)->first(),
            'roles' => $roles,
            'permissions' => $permissions,
            'userRoles' => $userRoles,
            'userPerms' => $userPerms
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, User $user)
    {
        $user = $user->where('id', $id)->first();
        $user->syncPermissions($request->get('permission'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
