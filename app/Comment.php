<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Parsedown;
use \App\Post;
use \App\User;

class Comment extends Model
{
    public $fillable = ['comment'];
    public function user() {
        return $this->belongsTo('\App\User')->withDefault();
    }
    public static function parse($comment) {
        $c = self::decode($comment);
        $parse = new Parsedown();
        $parse->setSafeMode(true);
        $parsed = $parse->text($c);

        return $parsed;
    }
    public static function decode($comment) {
        $c = htmlspecialchars_decode($comment);
        return $c;
    }
    public function getPost($comment) {
        return Post::where('id', $comment->post_id)->first();
    }

    public static function excerpt($comment) {
        $summary= array_chunk( preg_split('/\n/', $comment), 5);
        $summary=$summary[0];
        $summary= self::parse(implode($summary));
        return strip_tags($summary);
    }
}
