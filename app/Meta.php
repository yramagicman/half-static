<?php
namespace App;

interface Meta
{
    public static function makeSlug($name);
}
